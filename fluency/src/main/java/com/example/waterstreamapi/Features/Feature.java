package com.example.waterstreamapi.Features;

import java.util.Vector;

public class Feature <T extends Vector>{

    private final T data;

    // Unique name identifier
    final String name;

    public Feature(String name, T data) {
        this.name = name;
        this.data = data;
    }

    public T getData(){
        return this.data;
    }

    public static class Builder<T extends Vector> {

        String name;
        T data;

        public Builder(){}

        public Builder setName(String name){
            this.name = name;
            return this;
        }

        public Builder setData(T data){
            this.data = data;
            return this;
        }


        public Feature build(){
            return new Feature<>(name, data);
        }

    }

}
