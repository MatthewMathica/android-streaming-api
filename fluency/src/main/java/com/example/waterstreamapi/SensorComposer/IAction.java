package com.example.waterstreamapi.SensorComposer;

public interface IAction<I,O> {
    O perform(I a);
}
