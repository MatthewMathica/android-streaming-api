package com.example.myapplication;


import com.example.waterstreamapi.SensorComposer.IComposer;
import com.example.waterstreamapi.SensorComposer.VirtualSensor;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.function.Predicate;

@RunWith(JUnit4.class)
public class VirtualSensorTest {


    @Test
    public void predicate_addPredicate(){

        Predicate<Integer> predicate = p -> p < 10;

        IComposer node = VirtualSensor.builder()
               .using(new Object())
               .when(predicate)
               .then(a -> 10);
        /*
        * IComposer accelerometer = VirtualSensor.builder().
        *   .pipe(Transformation::Identity)
        *   .source(Motion.Accelerometer)
        *
        * IComposer gyroscope = VirtualSensor.builder()
        *   .pipe(Transformation::Identity)
        *   .source(Motion.Gyroscope)
        *
        * IComposer virtualSensor = VirtualSensor.builder()
        *   .using(accelerometer)
        *       .transform(Transformation::SmoothFilter)
        *       .when(a -> a.speed > Walking.SPEED)
        *   .using(gyroscope)
        *       .pipe(Transformation::SmoothFilter)
        *       .filter(a -> a.x > 0.5f);
        *   .aggregate(Feature::OrderedVector)
        *
        * */
    }


}
