package com.example.waterstreamapi.SensorComposer;

import java.util.function.Predicate;

public interface IComposer {

    IComposer when(Predicate predicate);
    IComposer using(Object data);
    IComposer then(IAction action);

}
