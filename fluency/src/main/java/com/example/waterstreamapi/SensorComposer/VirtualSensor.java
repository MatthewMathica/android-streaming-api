package com.example.waterstreamapi.SensorComposer;


import java.util.function.Predicate;

/**
* <h1>VirtualSensor class</h1>
 * <p> Represent builder for composite sensor definitions</p>
* @since 15-05-2019
* @author Matthew Matt
* @version 1.0
* */


// TODO : Consider dependency injector
public class VirtualSensor {

    private VirtualSensor(){}

    public static Builder builder(){
        return new Builder();
    }


    public static class Builder implements IComposer{


        public Builder(){}


        @Override
        public IComposer when(Predicate predicate) {
            return this;
        }

        @Override
        public IComposer using(Object data) {
            return null;
        }

        @Override
        public IComposer then(IAction action) {
            return null;
        }
    }

}
