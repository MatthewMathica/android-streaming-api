package com.example.myapplication;

import com.example.waterstreamapi.Configuration.Configuration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class ConfigurationTest {

    @Test
    public void boundary_sequencingOrderIsCorrect(){

        Configuration configuration = Configuration.builder()
                .setConfigurationName("hee")
                .addAttribute("a", new Object())
                .addAttribute("b", new Integer(1))
                .build();

    }

    @Test
    public void boundary_sequencingMandatoryIsCorrect(){
        Configuration configuration = Configuration.builder()
                .setConfigurationName("hee")
                .build();

    }


}
