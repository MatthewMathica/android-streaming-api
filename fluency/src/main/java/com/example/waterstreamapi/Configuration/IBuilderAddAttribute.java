package com.example.waterstreamapi.Configuration;

public interface IBuilderAddAttribute {

    IBuilderAddAttribute addAttribute(String attr, Object o);
    Configuration build();
}
