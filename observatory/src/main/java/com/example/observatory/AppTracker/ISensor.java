package com.example.observatory.AppTracker;

public interface ISensor {
    DataPipe register();
    boolean unregister();
}
