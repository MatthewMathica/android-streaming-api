package com.example.observatory.AppResources;

public class DeviceState {
    private static final DeviceState ourInstance = new DeviceState();

    public static DeviceState getInstance() {
        return ourInstance;
    }

    private DeviceState() {
    }
}
