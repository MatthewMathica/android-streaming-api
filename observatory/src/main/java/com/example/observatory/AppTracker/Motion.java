package com.example.observatory.AppTracker;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorListener;
import android.hardware.SensorManager;
import android.os.MemoryFile;

import com.example.observatory.BuildConfig;

import java.util.List;
import java.util.Map;
import java.util.function.Consumer;


/**
 * <h1> Motion class </h1>
 * <p>Responsible for registering, unregistering and providing Data from sensory measurements</p>
 * @see com.example.observatory.AppTracker.ISensor
 * @since 15-05-2019
 * @author Matthew Matt
 */
public class Motion extends DataPipe implements ISensor {

    private final Context context;
    private final SensorManager sensorManager;
    private final List<Sensor> deviceSensors;

    private Map<Sensor, SensorEventListener> sensorPumpMap;

    /**
     * Initialize available sensor list
     * Initialize SensorManager
     * @param context Application Context (Activity)
     * @see SensorManager
     */
    public Motion(Context context){
        this.context = context;

        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);

        deviceSensors = sensorManager.getSensorList(Sensor.TYPE_ALL);
    }

    // TODO: Create directChannel for sensors
    private boolean createDirectChannel(MemoryFile memoryFile) throws UnsupportedOperationException{
        throw new UnsupportedOperationException("Not implemented");
    }

    /**
     * <p> Registering sensor </p>
     * @param sensor Specifying sensor abstraction to register
     * @param listener DataSource of sensor data
     * @param frequency Frequency integer enumeration
     * @return boolean Information about successfully registering MotionSensor
     */
    private boolean registerMotionSensor(Sensor sensor,
                                         SensorEventListener listener,
                                         final int frequency){
        // Register (sensor, listener)
        sensorPumpMap.put(sensor, listener);

        return sensorManager.registerListener(listener, sensor, frequency);
    }

    /**
     * <p> Unregistering sensor </p>
     * @param sensor Specifying sensor abstraction to unregister
     */
    private void unregisterSensor(Sensor sensor){

        // Sensor -> SensorEventListener
        SensorEventListener listener = sensorPumpMap.get(sensor);

        sensorManager.unregisterListener(listener);
    }


    @Override
    public DataPipe register() {

        deviceSensors.stream().forEach(sensor -> registerMotionSensor(sensor, new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {

            }


            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {}
        }, SensorManager.SENSOR_DELAY_FASTEST));

        return this;
    }

    @Override
    public boolean unregister() {
        return false;
    }

    @Override
    public DataPipe merge(DataPipe a, DataPipe b) {
        return null;
    }
}
