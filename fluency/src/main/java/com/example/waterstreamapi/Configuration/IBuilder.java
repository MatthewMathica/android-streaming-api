package com.example.waterstreamapi.Configuration;

public interface IBuilder {

    IBuilderAddAttribute addAttribute(String attr, Object o);
    IBuilderAddAttribute setConfigurationName(String configurationName);

}
