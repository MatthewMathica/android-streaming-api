# Observatory API 

<img src="images/Logo.png" width="100" height="100">


### Overview
Android Library that simplify creation of device/user/context/behavior oriented events.
It's existence is based on pipes & filters with inherited fluency style.

### Modules

  * Observatory API - [Android Library]
  * WaterStream API (Streaming/Batching support) - [Java 8 Library]


### TODO
  
  * [x] Interface definition 
  * [ ] Configuration layer
  * [ ] Permission handling
  * [ ] Built.In Sensors
    * [ ] Network 
    * [x] Motion
    * [ ] Environment
    * [ ] Positional
  * [ ] Adaptative Streaming
  * [ ] Semantic builder
  
### Observatory Overview

DSL declaration

```
Sensor = .in Transform .out Sensor | e
Transform = .map Transform| .when Transform | .then Transform | e

```


Sensory DSL  example

```java

         IComposer accelerometer = VirtualSensor.builder()
           .in(Built.AccelerometerRaw)
           .out(Feature::OrderedVector);
         
         IComposer gyroscope = VirtualSensor.builder()
           .in(Built.Gyroscope)
           .map(Transformation::Identity)
           .out(Feature::ZScore);
        
         IComposer mixedGyroAcc = VirtualSensor.builder()
           .in(accelerometer)
               .map(Transformation::SmoothFilter)
               .when(a -> a.speed > Walking.SPEED)
           .in(gyroscope)
               .map(Transformation::SmoothFilter)
               .map(a -> a.x > 0.5f);
           .out(Feature::OrderedVector);
       
       CompoundNetwork
       .add(accelerometer)
       .add(gyroscope)
       .add(mixedGyroAcc)
```

    
