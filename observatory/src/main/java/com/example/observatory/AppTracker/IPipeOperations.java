package com.example.observatory.AppTracker;

public interface IPipeOperations {

    DataPipe merge(DataPipe a, DataPipe b);

}
