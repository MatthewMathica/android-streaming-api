package com.example.waterstreamapi.Configuration;

public interface IBuilderSetConfigurationName {

    IBuilderAddAttribute setConfigurationName(String configurationName);
}
