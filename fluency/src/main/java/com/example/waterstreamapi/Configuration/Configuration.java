package com.example.waterstreamapi.Configuration;

/*
*
* Configuration class
* Represents configuration attributes
*
* @author Matthew Matt
* */

import java.util.HashMap;

public class Configuration {

    private final HashMap<String, Object> table;
    private String serializedConfiguration;
    private final String configurationName;

    private Configuration(String name, HashMap table){
        this.configurationName = name;
        this.table = table;
    }

    public String getSerializedConfiguration(){
        return new String();
    }

    public static IBuilderSetConfigurationName builder(){
        return new BuilderSetConfigurationName();
    }

    public static class BuilderSetConfigurationName implements IBuilderSetConfigurationName, IBuilderAddAttribute {

        String name;
        HashMap<String, Object> table = new HashMap<>();

        public BuilderSetConfigurationName(){}

        //  If configuration name was set, return interface without setConfigurationName method
        @Override
        public IBuilderAddAttribute setConfigurationName(String configurationName)
        {
            this.name = configurationName;
            return this;
        }

        @Override
        public IBuilderAddAttribute addAttribute(String attr, Object o) {
            this.table.put(attr, o);
            return this;
        }

        @Override
        public Configuration build(){
            return new Configuration(name, table);
        }
    }

}
