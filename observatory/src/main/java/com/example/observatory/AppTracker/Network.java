package com.example.observatory.AppTracker;


import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.Message;
import android.util.Pair;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.karumi.dexter.listener.single.PermissionListener;

import java.util.List;
import java.util.Map;


public class Network extends DataPipe implements ISensor {


    private final WifiManager wifiManager;
    private final Activity context;

    // Network events
    BroadcastReceiver eventReceiver;

    // Registered events
    private Map<String, Pair<Handler,Intent>> networkEventFilter;


    public Network(Activity context){
        this.context = context;
        this.wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
    }


    public void registerWifiEvents(){

        eventReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String eventName = intent.getAction();

                Pair<Handler, Intent> performAction = networkEventFilter.get(eventName);

                if(performAction != null){

                    Message msg = new Message();
                    msg.obj = intent.getAction();

                    performAction.first.handleMessage(msg);
                }
            }
        };

    }

    @Override
    public DataPipe merge(DataPipe a, DataPipe b) {
        return null;
    }

    @Override
    public DataPipe register() {


        // Ger required permissions and handle states
        Dexter.withActivity(context).withPermission(Manifest.permission_group.LOCATION).withListener(new PermissionListener() {
            @Override
            public void onPermissionGranted(PermissionGrantedResponse response) {

            }

            @Override
            public void onPermissionDenied(PermissionDeniedResponse response) {

            }

            @Override
            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {

            }
        }).check();

        registerWifiEvents();

        return this;
    }

    @Override
    public boolean unregister() {
        return false;
    }
}
